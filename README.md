# gomazon

## Getting Started

### Start without Docker Compose:
Wait a few seconds after the Database is up! (Database uses Docker)
```
./scripts/start-mariadb.sh
cd src/gomazon && go run main.go

```
To close the Database use:
```
./scripts/stop-mariadb.sh

```

### Build, Start and Stop service using Docker Compose

```
docker compose build
docker compose up
docker compose down
```

## Technical Highlights
This service does not use Gorilla as framework. Here will be Gin-Gonic used. Gin is the most popular framework used by Golang-Developers. It is an extremely fast web framework and it suits the requirements of developers when they create microservices and web applications. (https://github.com/gin-gonic/gin)

## All API Functions
In this section all API-Functions will be presented with an example call!

### BASIC FUNCTIONS
This section contains all basic Functions.

#### GET JWT-Token
Use the Result-Token for all marked API-Functions!
This Token contains the Username and an isAdmin-Flag(true or false).
All Token-required Functions extract these information from the Token-String.
One Token is valid for 30 minutes!
```
curl --location --request GET 'localhost:8080/createJWT' \
--header 'Content-Type: application/json' \
--data '{
    "username": "user",
    "isAdmin": true
}'
```

#### Health Check
Function tests if the service is running.
```
curl --location 'localhost:8080/health'
```

### Product-Functions

#### CREATE PRODUCT (Admin only) (TOKEN REQUIRED)
This admin-only-Function adds a new Product with the name "WirePods", description "Kabellose Kopfhörer" and 130.20 as the price. The Totalrating is initial always 0.0!
```
curl --location 'localhost:8080/products' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer INSERT_TOKEN_HERE' \
--data '{
    "name": "WirePods",
    "description": "Kabellose Kopfhörer",
    "price": 130.20
}'
```

#### UPDATE PRODUCT (Admin only) (TOKEN REQUIRED)
This Function updates the Product with the ID = 1: the new Name is "WirePodsv2", description "Kopfhörer mit mehr Akkuleistung" and a new price (100.00).
```
curl --location --request PUT 'localhost:8080/products/1' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer INSERT_TOKEN_HERE' \
--data '{
    "name": "WirePodsv2",
    "description": "Kopfhörer mit mehr Akkuleistung",
    "price": 100.00
}'
```

#### DELETE PRODUCT (Admin only) (TOKEN REQUIRED)
This Function deletes the Product with the ID = 1.
```
curl --location --request DELETE 'localhost:8080/products/1' \
--header 'Authorization: Bearer INSERT_TOKEN_HERE'
```

#### GET ALL PRODUCTS
To retrieve all avaiable Products, use this Function:
```
curl --location 'localhost:8080/products'
```

#### GET SINGLE PRODUCT
To retrieve a specific Product (here Product with the ID = 1), use this Function:
```
curl --location 'localhost:8080/products/1'
```

### Rating-Functions

#### POST RATING (TOKEN REQUIRED)
With this Function a new Rating will be added. The username will be extracted from the Token-String. The Totalrating of this Product (here ID = 1) will be calculated by all Ratings of this Product (mean).
```
curl --location 'localhost:8080/products/1/rating' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer INSERT_TOKEN_HERE' \
--data '{
    "Content": "Great Product!",
    "Rating": 5
}'
```

#### DELETE RATING (TOKEN REQUIRED)
Only Admins and Owner of the Rating can use this! In this example, the rating with the ID = 3 will be deleted. If the request contains a Token-String with an Non-Admin or Token.Username != Rating.Owner nothing will happen. 
```
curl --location --request DELETE 'localhost:8080/ratings/3' \
--header 'Authorization: Bearer INSERT_TOKEN_HERE'
```

#### READ ALL RATINGS
To get all Ratings of all Products, use this Get-Function:
```
curl --location 'localhost:8080/ratings'
```

### Bankaccount-Functions

#### CREATE BANKACCOUNT (TOKEN REQUIRED)
If one user has already an account, the existing account is the result value.
The Owner will be detected by the Token-String
```
curl --location 'localhost:8080/bankaccounts' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer INSERT_TOKEN_HERE' \
--data '{
    "iban": "DE1234",
    "bankName": "Sparkasse",
    "balance": 200.00
}'
```

#### Deposit or withdraw (TOKEN REQUIRED)
To increase or decrease the Accountbalance, this Function can be used.
The Bankaccount will be detected by the username in the Token-String.
```
curl --location --request PUT 'localhost:8080/bankaccounts/transfer' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer INSERT_TOKEN_HERE' \
--data '{
  "amount": 50.0
}'
```

### ShoppingCart-Functions

#### CREATE SHOPPINGCART (TOKEN REQUIRED)
If one user has already an ShoppingCart, the existing ShoppingCart is the result value.
The user will also be detected with the Token-String.
```
curl --location --request POST 'localhost:8080/shoppingcart' \
--header 'Authorization: Bearer INSERT_TOKEN_HERE'
```

#### ADD PRODUCT TO SHOPPINGCART (TOKEN REQUIRED)
To add a Product (here Product with the ID = 1) use this function. In the request Body the amount will be specified.
The user will also be detected with the Token-String.
```
curl --location 'localhost:8080/shoppingcart/product/1' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer INSERT_TOKEN_HERE' \
--data '{
  "amount": 3
}'
```

#### PAY SHOPPINGCART (TOKEN REQUIRED)
With this Function, all Products will be buyed. If the Balance is lesser than the Totalprice, the Payment process will be canceled.
If the Payment process is successfull, the Shoppingcart will be deleted.
The user will also be detected with the Token-String.
```
curl --location --request POST 'localhost:8080/shoppingcart/pay' \
--header 'Authorization: Bearer INSERT_TOKEN_HERE'
```

### Testing the API-Calls
Because of the frequent specification of the JWT token, API calls with CURL can be bothersome. Therefore a postman collection can be imported with the file `go-mazon.postman_collection.json`. Import this to your Postman for more comfortable tests.

## Troubleshooting
If Docker doesn't detect a specific file, check if in VS-Code all Docker-related files has LF (Line Feed) and not CRLF (Carriage Return).