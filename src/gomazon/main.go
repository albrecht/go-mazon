package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.reutlingen-university.de/albrecht/gomazon/handler"
	"gitlab.reutlingen-university.de/albrecht/gomazon/service"
)

func main() {
	router := gin.Default()

	// create Service
	ProductService := &service.ProductService{}
	RatingService := &service.RatingService{}
	bankAccountService := &service.BankAccountService{}
	ShoppingCartService := &service.ShoppingCartService{}

	// create Handler
	ProductHandler := &handler.ProductHandler{ProductService: ProductService}
	RatingHandler := &handler.RatingHandler{RatingService: RatingService}
	bankAccountHandler := &handler.BankAccountHandler{BankAccountService: bankAccountService}
	ShoppingCartHandler := &handler.ShoppingCartHandler{ShoppingCartService: ShoppingCartService}

	// Initialer Test, wie Gin funktioniert
	router.GET("/health", func(c *gin.Context) {
		handler.Health(c.Writer, c.Request)
	})

	router.GET("/createJWT", handler.GenerateAdminTokenHandler) //generate jwt-token

	//Products
	router.POST("/products", ProductHandler.CreateProduct)       //new Product (Admin only)
	router.PUT("/products/:id", ProductHandler.UpdateProduct)    //update Product (Admin only)
	router.DELETE("/products/:id", ProductHandler.DeleteProduct) //Delete Product (Admin only)
	router.GET("/products", ProductHandler.ReadProducts)         //all Products
	router.GET("/products/:id", ProductHandler.GetProductByID)   //single Product

	//Rating
	router.POST("/products/:id/rating", RatingHandler.CreateRating) //new Rating
	router.DELETE("/ratings/:id", RatingHandler.DeleteRating)       //Delete Rating (Admin only (or own Rating))
	router.GET("/ratings", RatingHandler.ReadRatings)               //all Ratings

	//Bankaccount
	router.POST("/bankaccounts", bankAccountHandler.CreateBankAccount)         // new Bankaccount
	router.PUT("/bankaccounts/transfer", bankAccountHandler.UpdateBankAccount) // Transfer

	//ShoppingCart
	router.POST("/shoppingcart", ShoppingCartHandler.CreateShoppingCart)                   //new ShoppingCart
	router.POST("/shoppingcart/product/:id", ShoppingCartHandler.AddProductToShoppingCart) //add Product
	router.POST("/shoppingcart/pay", ShoppingCartHandler.TransferMoney)                    //pay

	router.Run(":8080")
}
