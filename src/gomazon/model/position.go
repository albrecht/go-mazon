package model

import "gorm.io/gorm"

type Position struct {
	gorm.Model
	Amount     int     `gorm:"notNull"`
	ProductId  uint    `gorm:"notNull"`
	Price      float64 `gorm:"notNull"`
	Totalprice float64 `gorm:"notNull"`
	//ShoppingcartId uint    `gorm:"notNull"`
}
