package model

import "gorm.io/gorm"

type Product struct {
	gorm.Model
	Name        string   `gorm:"notNull"`
	Description string   `gorm:"notNull;size:80"`
	Totalrating float64  `gorm:"notNull;default:0.0"`
	Price       float64  `gorm:"notNull"`
	Ratings     []Rating `gorm:"foreignKey:ProductId;constraint:OnUpdate:CASCADE,OnDelete:CASCADE"`
}
