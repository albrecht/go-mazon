package model

import "gorm.io/gorm"

type Rating struct {
	gorm.Model        //benötigt, weil Ratings auch gelöscht werden sollen
	Username   string `gorm:"notNull"`
	Content    string `gorm:"notNull"`
	Rating     int    `gorm:"notNull"`
	ProductId  uint   `gorm:"notNull"`
}
