package model

import "gorm.io/gorm"

type BankAccount struct {
	gorm.Model
	Iban     string  `gorm:"notNull"`
	BankName string  `gorm:"notNull"`
	Balance  float64 `gorm:"notNull"`
	Username string  `gorm:"unique"`
}
