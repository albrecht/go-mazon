package model

import "gorm.io/gorm"

type ShoppingCart struct {
	gorm.Model
	Positions  []Position `gorm:"foreignKey:ID;constraint:OnUpdate:CASCADE,OnDelete:CASCADE"`
	Totalprice float64    `gorm:"notNull;default:0.0"`
	Username   string     `gorm:"notNull"`
}
