package service

import (
	"fmt"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	log "github.com/sirupsen/logrus"
)

func GenerateAdminJWTToken(username string, isAdmin bool, expiration time.Time) (string, error) {
	claims := jwt.MapClaims{
		"username": username,
		"isAdmin":  isAdmin,
		"exp":      expiration.Unix(),
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	// Generiere den Token mit einem geheimen Schlüssel
	tokenString, err := token.SignedString([]byte("key-replacement"))
	if err != nil {
		log.Error("Stringsigning failed")
		return "", err
	}
	log.Info("JWT-Token generated")
	return tokenString, nil
}

// Extract Token
func ExtractTokenData(tokenString string) (string, bool, error) {
	tokenString = strings.TrimPrefix(tokenString, "Bearer ")
	// Parse Token
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return []byte("key-replacement"), nil
	})
	if err != nil {
		log.Error("Tokenparsing failed")
		return "", false, err
	}

	// Check if token is valid
	if !token.Valid {
		log.Error("invalid token")
		return "", false, fmt.Errorf("invalid token")
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok {
		expirationTime := time.Unix(int64(claims["exp"].(float64)), 0)
		if time.Now().After(expirationTime) {
			log.Errorf("Token has expired - Expiredate: %v - Time now: %v", expirationTime, time.Now())
			return "", false, fmt.Errorf("token has expired - Expiredate: %v - Time now: %v", expirationTime, time.Now())
		}
	} else {
		log.Error("Invalid token claims")
		return "", false, fmt.Errorf("invalid token claims")
	}

	// Extract claims
	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		log.Error("Invalid token claims")
		return "", false, fmt.Errorf("invalid token claims")
	}

	// check username and isAdmin
	username, ok := claims["username"].(string)
	if !ok {
		log.Error("Invalid Username claim")
		return "", false, fmt.Errorf("invalid username claim")
	}

	isAdmin, ok := claims["isAdmin"].(bool)
	if !ok {
		log.Error("Invalid isAdmin claim")
		return "", false, fmt.Errorf("invalid isAdmin claim")
	}

	return username, isAdmin, nil
}
