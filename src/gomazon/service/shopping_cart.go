package service

import (
	"errors"

	log "github.com/sirupsen/logrus"
	"gitlab.reutlingen-university.de/albrecht/gomazon/db"
	"gitlab.reutlingen-university.de/albrecht/gomazon/model"
	"gorm.io/gorm"
)

type ShoppingCartService struct {
	BankAccountService *BankAccountService
}

// Generate ShoppingCart (or get existing ShoppingCart)
func (s *ShoppingCartService) CreateShoppingCart(ShoppingCart model.ShoppingCart) (*model.ShoppingCart, error) {
	// Check if user has already a shoppingcart
	existingShoppingCart := &model.ShoppingCart{}
	result := db.DB.Where("username = ?", ShoppingCart.Username).First(existingShoppingCart)
	if result.Error == nil {
		return existingShoppingCart, nil
	} else if !errors.Is(result.Error, gorm.ErrRecordNotFound) {
		return nil, result.Error
	}

	result = db.DB.Create(&ShoppingCart)
	if result.Error != nil {
		log.Error("Could not create ShoppingCart")
		return nil, result.Error
	}

	log.Info("Successfully created Shoppingcart")
	return &ShoppingCart, nil
}

// Add single Product to ShoppingCart
func (s *ShoppingCartService) AddProductToShoppingCart(username string, ProductID uint, amount int) (*model.ShoppingCart, error) {
	//Get ShoppingCart
	ShoppingCart := &model.ShoppingCart{}
	result := db.DB.Preload("Positions").Where("username = ?", username).First(ShoppingCart)
	if result.Error != nil {
		log.Errorf("Could not find Shoppingcart of User %v", username)
		return nil, result.Error
	}

	// Get product information
	Product := &model.Product{}
	result = db.DB.First(Product, ProductID)
	if result.Error != nil {
		log.Errorf("Could not find Product with ID %v", ProductID)
		return nil, result.Error
	}

	// Generate total Price
	gesPrice := Product.Price * float64(amount)

	position := model.Position{
		Amount:     amount,
		ProductId:  Product.ID,
		Price:      Product.Price,
		Totalprice: gesPrice,
		//ShoppingcartId: ShoppingCart.ID,
	}

	// Add position to shoppingcart
	ShoppingCart.Positions = append(ShoppingCart.Positions, position)
	ShoppingCart.Totalprice += position.Totalprice

	result = db.DB.Save(ShoppingCart)
	if result.Error != nil {
		log.Error("Could not save Shoppingcart")
		return nil, result.Error
	}

	log.Info("Successfully added Product")
	return ShoppingCart, nil
}

// Tansfer Money
func (s *ShoppingCartService) TransferMoney(username string) error {
	// Get shoppingcart from user
	ShoppingCart := &model.ShoppingCart{}
	result := db.DB.Preload("Positions").Where("username = ?", username).First(ShoppingCart)
	if result.Error != nil {
		log.Errorf("Could not find Shoppingcart of User %v", username)
		return result.Error
	}

	// Get Bankacc from user
	bankAccount, err := s.BankAccountService.GetBankAccountByUsername(username)
	if err != nil {
		return err
	}

	// Check if Balance ge Totalprice
	if bankAccount.Balance <= ShoppingCart.Totalprice {
		log.Error("Not enough balance in the bank account")
		return errors.New("not enough balance in the bank account")
	}

	// Transfer Money
	_, err = s.BankAccountService.UpdateBankAccount(username, -ShoppingCart.Totalprice)
	if err != nil {
		log.Error("UpdateBankAccount failed")
		return err
	}

	result = db.DB.Delete(ShoppingCart)
	if result.Error != nil {
		log.Error("Could not delete Shoppingcart")
		return result.Error
	}

	log.Info("Successfully paid Shoppingcart")
	return nil
}
