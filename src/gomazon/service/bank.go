package service

import (
	"errors"
	"fmt"

	log "github.com/sirupsen/logrus"
	"gitlab.reutlingen-university.de/albrecht/gomazon/db"
	"gitlab.reutlingen-university.de/albrecht/gomazon/model"
	"gorm.io/gorm"
)

type BankAccountService struct{}

// Generate Bankaccount
func (s *BankAccountService) CreateBankAccount(bankAccount model.BankAccount) (*model.BankAccount, error) {
	//Check if user has already a Bankaccount
	existingAccount := &model.BankAccount{}
	result := db.DB.Where("username = ?", bankAccount.Username).First(existingAccount)
	if result.Error == nil {
		return existingAccount, nil
	} else if !errors.Is(result.Error, gorm.ErrRecordNotFound) {
		return nil, result.Error
	}

	if bankAccount.Balance < 0 {
		log.Error("Balance must be a positive value")
		return nil, fmt.Errorf("balance must be a positive value")
	}

	result = db.DB.Create(&bankAccount)
	if result.Error != nil {
		log.Error("Could not create Bankaccount")
		return nil, result.Error
	}
	log.Info("Created Bankaccount")
	return &bankAccount, nil
}

// Banktransfer
func (s *BankAccountService) UpdateBankAccount(username string, amount float64) (*model.BankAccount, error) {
	// Get account for user
	bankAccount := &model.BankAccount{}
	result := db.DB.Where("username = ?", username).First(bankAccount)
	if result.Error != nil {
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			log.Errorf("Bankaccount of User %s not found", username)
		}
		return nil, result.Error
	}

	// Check if the amount would result in a negative account balance
	if amount < 0 && bankAccount.Balance+amount < 0 {
		log.Error("Withdrawal amount exceeds the balance on the bank account")
		return nil, fmt.Errorf("withdrawal amount exceeds the balance on the bank account")
	}

	// Make a deposit or withdrawal from the bank account
	bankAccount.Balance += amount

	result = db.DB.Save(bankAccount)
	if result.Error != nil {
		log.Error("Could not save changes of Bankaccount")
		return nil, result.Error
	}

	return bankAccount, nil
}

// Get an account by username
func (s *BankAccountService) GetBankAccountByUsername(username string) (*model.BankAccount, error) {
	bankAccount := &model.BankAccount{}
	result := db.DB.Where("username = ?", username).First(bankAccount)
	if result.Error != nil {
		log.Errorf("Could not find Bankaccount from User %v", username)
		return nil, result.Error
	}
	log.Infof("Bankaccount from User %v found", username)
	return bankAccount, nil
}
