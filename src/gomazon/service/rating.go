package service

import (
	log "github.com/sirupsen/logrus"
	"gitlab.reutlingen-university.de/albrecht/gomazon/db"
	"gitlab.reutlingen-university.de/albrecht/gomazon/model"
)

type RatingService struct {
	ProductService *ProductService
}

// Create Rating
func (s *RatingService) CreateRating(ProductID uint, Rating *model.Rating) error {
	Rating.ProductId = ProductID

	result := db.DB.Create(Rating)
	if result.Error != nil {
		log.Error("Could not create Rating")
		return result.Error
	}

	Product, err := s.ProductService.GetProductByID(ProductID)
	if err != nil {
		log.Errorf("Error in CreateRating: GetProductByID failed")
		return nil
	}

	Product.Ratings = append(Product.Ratings, *Rating)

	result = db.DB.Save(&Product)
	if result.Error != nil {
		log.Error("Could not save Product")
		return result.Error
	}

	// Recalculate Rating for this product
	err = RecalculateTotalratingForProduct(Product)
	if err != nil {
		log.Errorf("Recalculation failed")
		return nil
	}

	entry := log.WithField("ID", ProductID)
	entry.Info("Successfully added new rating to a product.")
	entry.Tracef("Stored: %v", ProductID)
	return nil
}

// Delete Rating
func (s *RatingService) DeleteRating(RatingID uint) error {
	Rating, err := s.ProductService.GetRatingByID(RatingID)
	if err != nil {
		log.Errorf("failed to get Rating: %v", err)
		return nil
	}

	result := db.DB.Delete(&Rating)
	if result.Error != nil {
		log.Error("Could not delete Rating")
		return result.Error
	}

	product, err := s.ProductService.GetProductByID(Rating.ProductId)
	if err != nil {
		log.Errorf("failed to get Product: %v", err)
		return nil
	}

	// Recalculate
	err = RecalculateTotalratingForProduct(product)
	if err != nil {
		log.Errorf("Recalculation failed")
		return nil
	}

	log.Info("Successfully deleted Rating")
	return nil
}

// Get single Rating
func (s *ProductService) GetRatingByID(id uint) (*model.Rating, error) {
	var Rating model.Rating

	result := db.DB.First(&Rating, id)
	if result.Error != nil {
		log.Error("Could not find Rating")
		return nil, result.Error
	}

	log.Info("Found Rating")
	return &Rating, nil
}

// Get all Ratings
func (s *ProductService) ReadRatings() ([]model.Rating, error) {
	var Ratings []model.Rating

	result := db.DB.Find(&Ratings)
	if result.Error != nil {
		log.Error("Failed to load all Ratings")
		return nil, result.Error
	}

	return Ratings, nil
}

// Function for the Product-Rating calculation
func RecalculateTotalratingForProduct(Product *model.Product) error {
	// Resetting Totalrating
	Product.Totalrating = 0.0

	// Get all Ratings with specific productid for calculation
	var Ratings []model.Rating
	err := db.DB.Where("Product_id = ?", Product.ID).Find(&Ratings).Error
	if err != nil {
		log.Error("Could not find Product")
		return err
	}

	// generate Mean
	numRatings := len(Ratings)
	if numRatings > 0 {
		totalRating := 0
		for _, Rating := range Ratings {
			totalRating += Rating.Rating
		}
		Product.Totalrating = float64(totalRating) / float64(numRatings)
	}

	err = db.DB.Save(Product).Error
	if err != nil {
		log.Error("Could not save Product")
		return err
	}

	log.Infof("Successfully recalculated Product %v", Product.ID)
	return nil
}

// If a Product will be deleted, all Ratings of this Product will be deleted
// (Workarount, weil die Ratings nicht dem Slice übergeben werden)
func (s *RatingService) DeleteRatingsForProduct(productID uint) error {
	// Get all Ratings from Product
	var ratings []model.Rating
	err := db.DB.Where("product_id = ?", productID).Find(&ratings).Error
	if err != nil {
		return err
	}

	if len(ratings) == 0 {
		return nil
	}
	// Delete
	err = db.DB.Delete(&ratings).Error
	if err != nil {
		return err
	}
	log.Infof("All Ratings deleted für Product %v", productID)
	return nil
}
