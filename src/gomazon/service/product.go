package service

import (
	log "github.com/sirupsen/logrus"
	"gitlab.reutlingen-university.de/albrecht/gomazon/db"
	"gitlab.reutlingen-university.de/albrecht/gomazon/model"
)

type ProductService struct{}

// Create Product
func (s *ProductService) CreateProduct(Product model.Product) (*model.Product, error) {
	var ratings []model.Rating
	Product.Ratings = ratings
	result := db.DB.Create(&Product)
	if result.Error != nil {
		log.Error("Could not create Product")
		return nil, result.Error
	}

	return &Product, nil
}

// Get all Products
func (s *ProductService) ReadProducts() ([]model.Product, error) {
	var Products []model.Product

	result := db.DB.Find(&Products)
	if result.Error != nil {
		log.Error("Could not find Products")
		return nil, result.Error
	}

	return Products, nil
}

// Update Product
func (s *ProductService) UpdateProduct(id uint, ProductChanges model.Product) (*model.Product, error) {
	Product, err := s.GetProductByID(id)
	if err != nil {
		return nil, err
	}

	Product.Name = ProductChanges.Name
	Product.Description = ProductChanges.Description
	Product.Price = ProductChanges.Price
	Product.Ratings = ProductChanges.Ratings

	result := db.DB.Save(&Product)
	if result.Error != nil {
		log.Error("Could not save Product")
		return nil, result.Error
	}

	return Product, nil
}

// Delete Product
func (s *ProductService) DeleteProduct(id uint) error {
	Product, err := s.GetProductByID(id)
	if err != nil {
		return err
	}

	result := db.DB.Delete(&Product)
	if result.Error != nil {
		log.Error("Could not delete Product")
		return result.Error
	}

	return nil
}

// Get single Product
func (s *ProductService) GetProductByID(id uint) (*model.Product, error) {
	var Product model.Product

	result := db.DB.First(&Product, id)
	if result.Error != nil {
		log.Error("Could not find Product")
		return nil, result.Error
	}

	return &Product, nil
}
