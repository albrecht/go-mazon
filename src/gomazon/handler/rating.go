package handler

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"gitlab.reutlingen-university.de/albrecht/gomazon/model"
	"gitlab.reutlingen-university.de/albrecht/gomazon/service"
)

type RatingHandler struct {
	RatingService  *service.RatingService
	ProductService *service.ProductService
}

// Create Rating
func (h *RatingHandler) CreateRating(c *gin.Context) {
	//Get username from Auth-String
	tokenString := c.GetHeader("Authorization")
	username, _, err := service.ExtractTokenData(tokenString)
	if err != nil {
		log.Errorf("Extraction of JWT-Token failed")
	}
	// Extract ProductID from URL
	ProductID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		log.Errorf("Invalid ID for a product")
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	// Get Information from Body
	var Rating *model.Rating
	if err := c.ShouldBindJSON(&Rating); err != nil {
		log.Errorf("ShouldBindJSON in CreateRating failed")
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Check if Rating is in [0;5]
	if Rating.Rating < 0 || Rating.Rating > 5 {
		log.Errorf("Rating %v is not in [0;5]", Rating.Rating)
		c.JSON(http.StatusBadRequest, gin.H{"error": "Rating is not in [0;5]"})
		return
	}

	Rating.Username = username

	// Create Rating
	err = h.RatingService.CreateRating(uint(ProductID), Rating)
	if err != nil {
		log.Errorf("Creation of Rating failed")
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	log.Info("Ration successfully created")
	c.JSON(http.StatusOK, Rating)
}

// Delete Rating
func (h *RatingHandler) DeleteRating(c *gin.Context) {

	// Extract Rating-ID from URL
	RatingID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		log.Errorf("Invalid Rating ID")
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid Rating ID"})
		return
	}

	// Get Rating by ID
	Rating, err := h.ProductService.GetRatingByID(uint(RatingID))
	if err != nil {
		log.Errorf("Invalid Rating ID")
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid Rating ID"})
		return
	}

	// Get information from Auth-String
	tokenString := c.GetHeader("Authorization")
	username, admin, err := service.ExtractTokenData(tokenString)
	if err != nil {
		log.Errorf("Extraction of JWT-Token failed")
	}
	// Check if User is Admin or owner of the Rating
	if admin || (Rating.Username == username) {

		// Delete Rating
		err = h.RatingService.DeleteRating(uint(RatingID))
		if err != nil {
			log.Error("Could not delete Rating")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}
		log.Info("Rating successfully deleted")
		c.JSON(http.StatusOK, gin.H{"message": "Rating successfully deleted"})

	} else {
		c.JSON(http.StatusBadRequest, gin.H{"error": "not authorized"})
		log.Errorf("%v (isAdmin = %v) is not an admin or the owner (%v)", username, admin, Rating.Username)
		return
	}
}

// Get all Ratings
func (h *RatingHandler) ReadRatings(c *gin.Context) {
	Products, err := h.RatingService.ProductService.ReadRatings()
	if err != nil {
		log.Errorf("Failed to load all Ratings")
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	log.Info("Loaded all ratings")
	c.JSON(http.StatusOK, Products)
}
