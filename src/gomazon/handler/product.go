package handler

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"gitlab.reutlingen-university.de/albrecht/gomazon/model"
	"gitlab.reutlingen-university.de/albrecht/gomazon/service"
)

type ProductHandler struct {
	ProductService *service.ProductService
	RatingService  *service.RatingService
}

// Create Product
func (h *ProductHandler) CreateProduct(c *gin.Context) {
	//Get information from Auth-String
	tokenString := c.GetHeader("Authorization")
	username, admin, err := service.ExtractTokenData(tokenString)
	if !admin { // Check if user is admin
		c.JSON(http.StatusBadRequest, gin.H{"error": "not authorized"})
		log.Errorf("%v is not an admin", username)
		return
	}
	if err != nil {
		log.Errorf("Extraction of JWT-Token failed")
		return
	}

	var Product model.Product

	// Bind information from Body
	if err := c.ShouldBindJSON(&Product); err != nil {
		log.Errorf("ShouldBindJSON in CreateProduct failed")
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	Product.Totalrating = 0.0

	// Create Product
	createdProduct, err := h.ProductService.CreateProduct(Product)
	if err != nil {
		log.Errorf("Creation of product failed")
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	log.Info("Product successfully created")
	c.JSON(http.StatusOK, createdProduct)
}

// Get all Products
func (h *ProductHandler) ReadProducts(c *gin.Context) {
	Products, err := h.ProductService.ReadProducts()
	if err != nil {
		log.Errorf("Failed to load all Products")
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	log.Info("Loaded all products")
	c.JSON(http.StatusOK, Products)
}

// Update single Product
func (h *ProductHandler) UpdateProduct(c *gin.Context) {
	// Get information from Auth-String
	tokenString := c.GetHeader("Authorization")
	username, admin, err := service.ExtractTokenData(tokenString)
	if !admin { //Check if user is an admin
		c.JSON(http.StatusBadRequest, gin.H{"error": "not authorized"})
		log.Errorf("%v is not an admin", username)
		return
	}
	if err != nil {
		log.Errorf("Extraction of JWT-Token failed")
		return
	}

	// Get ProductId from URL
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		log.Error("Invalid ID")
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid ID"})
		return
	}

	//Bind Bondy-informations to an object
	var Product model.Product
	if err := c.ShouldBindJSON(&Product); err != nil {
		log.Error("ShouldBindJSON in UpdateProduct failed")
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	updatedProduct, err := h.ProductService.UpdateProduct(uint(id), Product)
	if err != nil {
		log.Error("Productupdate failed")
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	log.Info("Product updated successfully")
	c.JSON(http.StatusOK, updatedProduct)
}

// Delete Product
func (h *ProductHandler) DeleteProduct(c *gin.Context) {
	// Get informations from Auth-String
	tokenString := c.GetHeader("Authorization")
	username, admin, err := service.ExtractTokenData(tokenString)
	if !admin { // Check if user is an admin
		c.JSON(http.StatusBadRequest, gin.H{"error": "not authorized"})
		log.Errorf("%v is not an admin", username)
		return
	}
	if err != nil {
		log.Errorf("Extraction of JWT-Token failed")
		return
	}

	// Get Productid from URL
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		log.Error("Invalid ID")
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid ID"})
		return
	}

	err = h.ProductService.DeleteProduct(uint(id))
	if err != nil {
		log.Error("DeleteProduct failed")
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	err = h.RatingService.DeleteRatingsForProduct(uint(id))
	if err != nil {
		log.Error(err.Error())
	}

	log.Info("Product successfully removed")
	c.JSON(http.StatusOK, gin.H{"message": "Product successfully removed"})
}

// Get single Product
func (h *ProductHandler) GetProductByID(c *gin.Context) {
	//get ProductId from URL
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		log.Error("Invalid ID")
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid ID"})
		return
	}

	Product, err := h.ProductService.GetProductByID(uint(id))
	if err != nil {
		log.Errorf("Could not find Product with the ID: %v", id)
		c.JSON(http.StatusNotFound, gin.H{"error": "Could not find Product"})
		return
	}
	log.Infof("Product with ID %v found", id)
	c.JSON(http.StatusOK, Product)
}
