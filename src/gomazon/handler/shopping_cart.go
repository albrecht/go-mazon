package handler

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"gitlab.reutlingen-university.de/albrecht/gomazon/model"
	"gitlab.reutlingen-university.de/albrecht/gomazon/service"
)

type ShoppingCartHandler struct {
	ShoppingCartService *service.ShoppingCartService
	BankAccountService  *service.BankAccountService
}

// Create ShoppingCart
func (h *ShoppingCartHandler) CreateShoppingCart(c *gin.Context) {
	//Get information from Auth-String
	tokenString := c.GetHeader("Authorization")
	username, _, err := service.ExtractTokenData(tokenString)
	if err != nil {
		log.Errorf("Extraction of JWT-Token failed")
		return
	}

	var ShoppingCart model.ShoppingCart
	// if err := c.ShouldBindJSON(&ShoppingCart); err != nil {
	// 	c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	// 	return
	// }
	ShoppingCart.Username = username

	createdShoppingCart, err := h.ShoppingCartService.CreateShoppingCart(ShoppingCart)
	if err != nil {
		log.Error("Could not create Shopping Cart")
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	log.Info("Shopping Cart created/Get existing Shopping Cart")
	c.JSON(http.StatusCreated, createdShoppingCart)
}

// Add Product
func (h *ShoppingCartHandler) AddProductToShoppingCart(c *gin.Context) {
	// Get information from Auth-String
	tokenString := c.GetHeader("Authorization")
	username, _, err := service.ExtractTokenData(tokenString)
	if err != nil {
		log.Errorf("Extraction of JWT-Token failed")
		return
	}

	//Get product id from url
	ProductID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		log.Error("Invalid Product ID")
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid Product ID"})
		return
	}

	var addProductData struct {
		Amount int `json:"amount" binding:"required"`
	}

	//bind information from body
	if err := c.ShouldBindJSON(&addProductData); err != nil {
		log.Error("ShouldBindJSON in AddProductToShoppingCart failed")
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	updatedShoppingCart, err := h.ShoppingCartService.AddProductToShoppingCart(username, uint(ProductID), addProductData.Amount)
	if err != nil {
		log.Error("Coud not insert product to Shopping Cart")
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	log.Info("Insertet Product to Shopping Cart")
	c.JSON(http.StatusOK, updatedShoppingCart)
}

// Bezahlfunktion für den ShoppingCart
func (h *ShoppingCartHandler) TransferMoney(c *gin.Context) {
	tokenString := c.GetHeader("Authorization")
	username, _, err := service.ExtractTokenData(tokenString)
	if err != nil {
		log.Errorf(err.Error())
	}

	err = h.ShoppingCartService.TransferMoney(username)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "Zahlung erfolgreich"})
}
