package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"gitlab.reutlingen-university.de/albrecht/gomazon/model"
	"gitlab.reutlingen-university.de/albrecht/gomazon/service"
)

type BankAccountHandler struct {
	BankAccountService *service.BankAccountService
}

// Generate Bankaccount
func (h *BankAccountHandler) CreateBankAccount(c *gin.Context) {
	//Get informations of Auth-String
	tokenString := c.GetHeader("Authorization")
	username, _, err := service.ExtractTokenData(tokenString)
	if err != nil {
		log.Errorf("Extraction of JWT-Token failed")
		return
	}

	//Generate Account object
	var bankAccount model.BankAccount
	if err := c.ShouldBindJSON(&bankAccount); err != nil {
		log.Errorf("ShouldBindJSON in CreateBankAccount failed")
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	bankAccount.Username = username

	//Create Account with object
	createdAccount, err := h.BankAccountService.CreateBankAccount(bankAccount)
	if err != nil {
		log.Errorf("Creation of the bank account failed")
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	log.Info("Bank account successfully created")
	c.JSON(http.StatusCreated, createdAccount)
}

// Banktransfer
func (h *BankAccountHandler) UpdateBankAccount(c *gin.Context) {
	//Get information of Auth-String
	tokenString := c.GetHeader("Authorization")
	username, _, err := service.ExtractTokenData(tokenString)
	if err != nil {
		log.Errorf("Extraction of JWT-Token failed")
	}

	//Body
	var withdrawalData struct {
		Amount float64 `json:"amount" binding:"required"`
	}

	//Generate Object
	if err := c.ShouldBindJSON(&withdrawalData); err != nil {
		log.Errorf("ShouldBindJSON in UpdateBankAccount failed")
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	//Update Bankbalance
	updatedAccount, err := h.BankAccountService.UpdateBankAccount(username, withdrawalData.Amount)
	if err != nil {
		log.Errorf("Account update from User '%v' failed", updatedAccount.Username)
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	log.Info("Account update successfull")
	c.JSON(http.StatusOK, updatedAccount)
}
