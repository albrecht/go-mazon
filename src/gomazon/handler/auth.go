package handler

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"gitlab.reutlingen-university.de/albrecht/gomazon/service"
)

// Handler-Function to generate Admin-JWT-Tokens
func GenerateAdminTokenHandler(c *gin.Context) {
	// Username and isAdmin of the Request-Body
	var requestBody struct {
		Username string `json:"username"`
		IsAdmin  bool   `json:"isAdmin"`
	}

	//Bind Body with requestBody struct
	if err := c.ShouldBindJSON(&requestBody); err != nil {
		log.Errorf("Invalid request body for authentication")
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	// Check username
	if requestBody.Username == "" {
		log.Errorf("Invalid username for authentication")
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid username"})
		return
	}

	// Check isAdmin
	if _, ok := interface{}(requestBody.IsAdmin).(bool); !ok {
		log.Errorf("Invalid isAdmin flag for authentication")
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid isAdmin value"})
		return
	}

	// Generate JWT-Token
	expiration := time.Now().Add(30 * time.Minute)
	token, err := service.GenerateAdminJWTToken(requestBody.Username, requestBody.IsAdmin, expiration)
	if err != nil {
		log.Errorf("Failed to generate admin token")
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to generate admin token"})
		return
	}
	log.Infof("Generated admin token")
	c.JSON(http.StatusOK, gin.H{"token": token})
}
