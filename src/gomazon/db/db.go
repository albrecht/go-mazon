package db

import (
	"fmt"
	"os"

	log "github.com/sirupsen/logrus"
	"gitlab.reutlingen-university.de/albrecht/gomazon/model"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var DB *gorm.DB

func init() {
	dsn := fmt.Sprintf("root:root@tcp(%s)/gomazon?charset=utf8&parseTime=True&loc=Local", os.Getenv("DB_CONNECT"))
	log.Info("UsingDSN for DB:", dsn)
	var err error
	DB, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("failed to connect to database")
	}
	log.Info("Starting automatic migrations")
	if err := DB.Debug().AutoMigrate(&model.BankAccount{}); err != nil {
		log.Errorf("Failed to auto migrate BankAccount")
		panic(err)
	}
	if err := DB.Debug().AutoMigrate(&model.Product{}); err != nil {
		log.Errorf("Failed to auto migrate Product")
		panic(err)
	}
	if err := DB.Debug().AutoMigrate(&model.ShoppingCart{}); err != nil {
		log.Errorf("Failed to auto migrate ShoppingCart")
		panic(err)
	}
	if err := DB.Debug().AutoMigrate(&model.Rating{}); err != nil {
		log.Errorf("Failed to auto migrate Rating")
		panic(err)
	}
	if err := DB.Debug().AutoMigrate(&model.Position{}); err != nil {
		log.Errorf("Failed to auto migrate Position")
		panic(err)
	}
	log.Info("Automatic migrations finished")
}
